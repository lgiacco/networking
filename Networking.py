import requests

headers = {'content-type' : 'application/x-www-form-urlencoded'}
running = True
ok_command = False

url = input("Enter the URL (Press ENTER to use the one by default): ")
if url == "":
    url = "https://luka-wsrestnotes.herokuapp.com"

while running:
    command = input("Enter the command (POST, PUT, GET, DELETE, EXIT): ").upper()
    if command == "GET":
        ok_command = False
        while not ok_command:
            aux_url = url
            search_type = input("What kind of query do you want to execute? (GENERAL, BY NAME, BY CONTENT): ").upper()
            if search_type == "GENERAL":
                ok_command = True
            elif search_type == "BY NAME":
                aux_url += "/" + input("Enter the note's name: ")
                ok_command = True
            elif search_type == "BY CONTENT":
                aux_url += "/content/" + input("Enter the note's content: ")
                ok_command = True
            else:
                print("Wrong command!")

        response = requests.get(aux_url)
        if response.status_code == 200:
            print("\n" + str(response.content))
        else:
            print("\nERROR")
            
    elif command == "POST":
        ok_command = False
        while not ok_command:
            name = input("Enter the note's name: ")
            if name != "":
                ok_command = True

        content = input("Enter the note's content: ")
        response = requests.post(url + "/" + name, data = {'content' : content}, headers = headers)
        
        if response.status_code == 200:
            print("\nSuccess!")
        else:
            print("\nERROR")
            
    elif command == "PUT":
        ok_command = False
        while not ok_command:
            name = input("Enter the note's name: ")
            if name != "":
                ok_command = True

        content = input("Enter the note's content: ")
        response = requests.put(url + "/" + name, data = {'content' : content}, headers = headers)
        
        if response.status_code == 200:
            print("\nSuccess!")
        else:
            print("\nERROR")

    elif command == "DELETE":
        ok_command = False
        while not ok_command:
            name = input("Enter the note's name: ")
            if name != "":
                ok_command = True

        response = requests.delete(url + "/" + name)
        
        if response.status_code == 200:
            print("\nSuccess!")
        else:
            print("\nERROR")

    elif command == "EXIT":
        running = False
